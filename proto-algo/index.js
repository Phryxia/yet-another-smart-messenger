const Soup = require('../common/algorithm/soup');
const Parser = require('../common/algorithm/parser');
const Parser2 = require('../common/algorithm/parser2');
const Quest = require('../common/algorithm/quest');
const Protocol = require('../common/algorithm/protocol');
const Mocktest = require('../common/algorithm/mocktest');
const Util = require('../common/algorithm/util');

/*
	모의고사 데모 예시
*/
// 참/거짓 문제를 위한 DOM을 만들어 반환한다.
function create_tfquest_dom(quest) {
	console.assert(quest.type == 'binary');

	let dom = document.createElement('div');
	dom.className = 'section';

	let stmt = document.createElement('p');
	stmt.innerText = `[${quest.title}]${quest.statement}`;
	dom.appendChild(stmt);

	let radio_t = document.createElement('input');
	radio_t.type = 'radio';
	radio_t.checked = (quest.answers[0] == 'T');
	dom.appendChild(radio_t);
	dom.appendChild(document.createTextNode(' T'));
	dom.appendChild(document.createElement('br'));

	let radio_f = document.createElement('input');
	radio_f.type = 'radio';
	radio_f.checked = (quest.answers[0] == 'F');
	dom.appendChild(radio_f);
	dom.appendChild(document.createTextNode(' F'));

	return dom;
}

// 4지선다 문제를 위한 DOM을 만들어 반환한다.
function create_selection_dom(quest) {
	console.assert(quest.type == 'selection' || quest.type == 'selection2');

	let dom = document.createElement('div');
	dom.className = 'section';

	let stmt = document.createElement('p');
	stmt.innerText = `[${quest.title}]${quest.statement}`;
	dom.appendChild(stmt);

	for(let i = 0; i < quest.choices.length; ++i) {
		let checkbox = document.createElement('input');
		checkbox.type = 'checkbox';
		checkbox.checked = (quest.answers.indexOf(`${i}`) != -1);
		dom.appendChild(checkbox);
		dom.appendChild(document.createTextNode(quest.choices[i]));
		dom.appendChild(document.createElement('br'));
	}

	return dom;
}

// 단답형 문제를 위한 DOM을 만들어 반환한다.
function create_short_dom(quest) {
	console.assert(quest.type == 'short');

	let dom = document.createElement('div');
	dom.className = 'section'
	
	let stmt = document.createElement('p');
	stmt.innerText = `[${quest.title}]${quest.statement}`;
	dom.appendChild(stmt);
	dom.appendChild(document.createElement('br'));

	let ans = document.createElement('input');
	ans.type = 'textarea';
	ans.value = quest.answers.toString();
	dom.appendChild(ans);

	return dom;
};

document.getElementById('docs').onchange = function(evt) {
	debug_parse_doc(evt.target.value);
};

let soup = null;

// 파싱 예제
function debug_parse_doc(docstr) {
	// parser2 test
	soup = Parser2.parse(docstr);
	console.log(soup);

	// for debug purpose
	let dom = document.querySelector('#tree');
	dom.value = soup.get_tree();
	// soup = Parser.parse_doc(docstr);
	// console.log(soup);

	// 토폴로지 테스트
	let msg = Protocol.create_message(soup, 'file');
	console.log('START TOPOLOGY TEST');
	console.log('ENCODED MESSAGE:');
	console.log(msg);

	let newsoup = Protocol.parse_message(msg);
	console.log('DECODED SOUP');
	console.log(newsoup);

	// dom.value += newsoup.get_tree();

	// let issues = soup.validation_check({n: 4, a: 1});
	// issues.forEach(issue => {
	// 	alert(issue.what);
	// 	console.log(issue.what);
	// });
}

// 모의고사 생성!
document.getElementById('mocktest').onclick = function() {
	if(!soup)
		return;

	// 문제출제 범위 선택
	let out_dom = document.getElementById('mocktest-out');
	out_dom.innerHTML = '';
	let n = parseInt(document.getElementById('mocktest-n').value);

	// 모의고사 문제 생성
	let mocktest = Mocktest.create_mocktest(soup.roots, n);
	console.log('문제출제 완료');
	console.log(mocktest);

	// 문제 유형에 맞는 DOM을 out_dom에 append한다.
	mocktest.quests.forEach(quest => {
		if(!quest)
			return;

		// Horizontal Line
		out_dom.appendChild(document.createElement('hr'));
		if(quest.type == 'binary')
			out_dom.appendChild(create_tfquest_dom(quest));
		else if(quest.type == 'selection' || quest.type == 'selection2')
			out_dom.appendChild(create_selection_dom(quest));
		else if(quest.type == 'short')
			out_dom.appendChild(create_short_dom(quest));
	});
}